# PyTetris

Тетрис, написанный на Python 3 с использованием PyGame

<img src='screen.jpg'>


## Установка и использование

```
git clone https://gitlab.com/k_v_s/pytetris.git
cd pytetris/
pip install -r requirements.txt
python tetris.py

```

## Об авторе
Автор: @k_v_s <br/>
Telegram: https://t.me/k_v_s_03 <br/>
Mail: kvs_2022@bk.ru <br/>

